FROM node:16-alpine

WORKDIR /work/simpleApp

COPY package*.json ./

COPY . .

EXPOSE 3000

CMD ["node", "app.js"]
